#PARAMETROS
param MAXCUATRI := 10;

#CONJUNTOS
set materias := {"IP", "IM", "TL", "P1", "P2"};
set dias := {"L","M","X","J","V","S"};
set cuatrimestres := {1..MAXCUATRI};
set turno :={"M","T","N"};

#ME CREO UN CONJUNTO CON LOS CUATRIMESTRES DE LAS MATERIAS QUE TENGAN CORRELATIVAS.
set C :={}; #ME CREO UN CONJUNTO

#EJ IP no tiene correlativas {}, P2 tiene como correlativa {"P1"}
set correlativas[materias] := <"IP"> {}, <"IM"> {}, <"TL"> {}, <"P1"> {"IP"}, <"P2"> {"P1"};

#set diasMaterias[materias] := <"IP"> {"L","J"}, <"IM"> {"M","V"}, <"TL"> {"X","J"}, <"P1"> {"L", "M"}, <"P2"> {"X", "V"};
#EJ IP se dicta en el cuatrimestre 1, p1 en el cuatrimestre 2
#set cuatrimestreMateria[materias] := <"IP"> {1}, <"IM"> {1}, <"TL"> {1}, <"P1"> {2}, <"P2"> {1};

#VARIABLES
var x[materias*cuatrimestres] binary;
var y[cuatrimestres] binary;  #Si me anoto al menos a una materia en el cuatrimestre.

#FUNCIÓN OBJETIVO
minimize tiempo: sum<i> in cuatrimestres: y[i];

#RESTRICCIONES
subto noMeanotoSinCorrelativas: 
		forall <c> in cuatrimestres:
			forall <i> in materias:
				forall <j> in correlativas[i]:
					x[i,c]<= x[j,c]; #Sino hice la materia j, no puedo hacer la materia i.

subto meAnotoaTodaslasmaterias: 
		forall <i> in materias:
			sum <c> in cuatrimestres:
				x[i,c] == 1;						

subto estoyCursandoAlmenosunaMateriaenelCuatrimestrec:
		forall <c> in cuatrimestres:
			forall <i> in materias:
				y[c]>= x[i,c];
						
				
subto sicurseenNtmbcurseenNmenos1:
		forall <c> in cuatrimestres with c>1:
				y[c]<=y[c-1];
				
#subto siTengocorrelativasMeanotoCuandoHayaCursadoTodas:
#		forall <i> in materias:
#				forall <j> in correlativas[i]: #ME ARMO UN CONJUNTO CON TODOS LOS CUATRIMESTRES EN LOS QUE ME ANOTE A LAS MATERIAS CORRELATIVAS.
#						C[j]= x[j,c]*c; #LO MULTIPLICO POR EL CUATRIMESTRE, ENTONCES SI ME ANOTE EN IP EL 4TO CUATRI GUARDO ==> 1 * 4
#			
#			#x[i,max(C)] == 1;	#ME ANOTO EN LA MATERIA QUE TENGA EL NÚMERO DE CUATRIMESTRE MAYOR.
					
		
subto testMaxi:
		x["P2",3]==1;